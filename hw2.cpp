#include "hw2.hpp"

#include <functional>
#include <algorithm>
#include <numeric>

LuckyTicket::LuckyTicket(uint half_of_N) :
    base_numeral_system(10),
    digits(half_of_N),
    trivial_case(half_of_N)
{
    uint all_possible_sums_for_half_of_N_digit = (base_numeral_system - 1) * digits + 1; // also count Zero-Sum
    lucky_tickets.resize(all_possible_sums_for_half_of_N_digit, 0); // initialize all elements by 0;
}

ull LuckyTicket::get()
{
    find(digits);
    calculate_number_of_ticket_for_each_sum();
    return number_of_lucky_tickets();
}

void LuckyTicket::calculate_number_of_ticket_for_each_sum()
{
    std::for_each(lucky_tickets.begin(), lucky_tickets.end(), [](auto &n) {n = std::pow(n, 2);});
}

ull LuckyTicket::number_of_lucky_tickets()
{
    return std::accumulate(lucky_tickets.begin(), lucky_tickets.end(), 0);
}

void LuckyTicket::find(uint N)
{
    if (N == 0)
        return;

    if (N == trivial_case)
    {
        std::fill_n(lucky_tickets.begin(), 10, 1);
    }
    else
    {
        auto begin = lucky_tickets.begin();
        auto end = begin + (base_numeral_system - 1) * (trivial_case - N) + 1;
        std::vector<ull> lucky_tickets_from_last_iteration(begin, end);
        sum(lucky_tickets_from_last_iteration, 1);
    }
    
    find(N-1);
}

void LuckyTicket::sum(const std::vector<ull> &addendum, uint offset)
{
    if (offset == base_numeral_system)
        return;

    auto range1_begin = lucky_tickets.begin() + offset;
    auto range1_end = range1_begin + addendum.size();
    auto range2_begin = addendum.begin();
    auto destination_begin = range1_begin;
    std::transform(range1_begin, range1_end, range2_begin, destination_begin, std::plus<ull>());

    sum(addendum, offset + 1);
}
