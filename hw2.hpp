#include <vector>
#include <cmath>

using ull = unsigned long long;

class LuckyTicket
{
    private:
        std::vector<ull> lucky_tickets;
        uint base_numeral_system;
        uint digits;
        const uint trivial_case;

        void calculate_number_of_ticket_for_each_sum();
        ull number_of_lucky_tickets();
        void find(uint N);
        void sum(const std::vector<ull> &addendum, uint offset);

    public:
        LuckyTicket(uint);
        LuckyTicket() = delete;
        ~LuckyTicket() = default;

        ull get();
};
