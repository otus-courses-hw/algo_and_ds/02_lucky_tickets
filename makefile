all: test
	./test

test: test.o hw2.o
	g++ -o $@ $^

%.o: %.cpp
	g++ -g -c $< -o $@

clean:
	rm *.o test

.PHONY: clean