#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>
#include "hw2.hpp"

TEST_CASE("one digit")
{
    LuckyTicket lk(1);
    REQUIRE(lk.get() == 10);
}
TEST_CASE("two digit")
{
    LuckyTicket lk(2);
    REQUIRE(lk.get() == 670);
}
TEST_CASE("three digit")
{
    LuckyTicket lk(3);
    REQUIRE(lk.get() == 55252);
}
TEST_CASE("four digit")
{
    LuckyTicket lk(4);
    REQUIRE(lk.get() == 4816030);
}
TEST_CASE("five digit")
{
    LuckyTicket lk(5);
    REQUIRE(lk.get() == 432457640);
}
TEST_CASE("six digit")
{
    LuckyTicket lk(6);
    REQUIRE(lk.get() == 39581170420);
}
TEST_CASE("seven digit")
{
    LuckyTicket lk(7);
    REQUIRE(lk.get() == 3671331273480);
}
TEST_CASE("eight digit")
{
    LuckyTicket lk(8);
    REQUIRE(lk.get() == 343900019857310);
}
TEST_CASE("nine digit")
{
    LuckyTicket lk(9);
    REQUIRE(lk.get() == 32458256583753952);
}
TEST_CASE("ten digit")
{
    LuckyTicket lk(6);
    REQUIRE(lk.get() == 3081918923741896840);
}
